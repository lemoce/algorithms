#ifndef BTREE_H
#define BTREE_H

#define M 50

typedef long Key;

typedef struct {
  long line;
  long int location;
} *Item;


typedef struct STnode *link;
typedef struct {
  Key key;
  union {
    link next;
    Item item;
  } ref;
} entry;

struct STnode {
  entry b[M];
  int m;
};

static link head;
static int H, N;

link NEW();
void STinit(int maxN);
Item STsearch(Key key);
void STinsert(Item item);

#endif /* BTREE_H */
