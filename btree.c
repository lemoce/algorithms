#include <stdio.h>
#include <stdlib.h>

#include "btree.h"

link NEW() {
  link x = malloc(sizeof *x);
  x->m = 0;
  return x;
}

void STinit(int maxN) {
  head = NEW();
  H = 0;
  N = 0;
}

Item searchR(link h, Key v, int H) {
  int j;
  if (H == 0) {
    for (j = 0; j < h->m; j++) {
      if (v == h->b[j].key) {
        return h->b[j].ref.item;
      }
    }
  }
  if (H != 0) {
    for (j = 0; j < h->m; j++) {
      if ((j + 1 == h->m) || (v < h->b[j + 1].key)) {
        return searchR(h->b[j].ref.next, v, H - 1);
      }
    }
  }
  return NULL;
}

Item STsearch(Key v) { return searchR(head, v, H); }

link split(link h) {
  int j;
  link t = NEW();
  for (j = 0; j < M / 2; j++) {
    t->b[j] = h->b[M / 2 + j];
  }
  h->m = M / 2;
  t->m = M / 2;
  return t;
}

link insertR(link h, Item item, int H) {
  int i, j;
  Key v = item->line;
  entry x;
  link t, u;

  x.key = v;
  x.ref.item = item;

  if (H == 0) {
    for (j = 0; j < h->m; j++) {
      if (v < h->b[j].key) {
        break;
      }
    }
  }
  if (H != 0) {
    for (j = 0; j < h->m; j++) {
      if ((j + 1 == h->m) || (v < h->b[j + 1].key)) {
        t = h->b[j++].ref.next;
        u = insertR(t, item, H - 1);
        if (u == NULL) {
          return NULL;
        }
        x.key = u->b[0].key;
        x.ref.next = u;
        break;
      }
    }
  }
  for (i = (h->m)++; i > j; i--) {
    h->b[i] = h->b[i - 1];
  }
  h->b[j] = x;
  N++;
  if (h->m < M) {
    return NULL;
  } else {
    return split(h);
  }
}

void STinsert(Item item) {
  link t, u = insertR(head, item, H);
  if (u == NULL) {
    return;
  }

  t = NEW();
  t->m = 2;
  t->b[0].key = head->b[0].key;
  t->b[0].ref.next = head;
  t->b[1].key = u->b[0].key;
  t->b[1].ref.next = u;
  head = t;
  H++;
}

int main(int argc, char *argv[]) {
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  long line_number = 0;

  fp = fopen("/home/lemoce/workspaces/demandas/senda/server-pix844.log", "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);

  STinit(0);

  Item my_item = malloc(sizeof(Item));
  my_item->line = ++line_number;
  my_item->location = ftell(fp);
  STinsert(my_item);

  while ((read = getline(&line, &len, fp)) != -1) {
    Item my_new_item = malloc(sizeof(Item));
    my_new_item->line = ++line_number;
    my_new_item->location = ftell(fp);
    STinsert(my_new_item);
  }

  fclose(fp);
  if (line)
    free(line);

  printf("Altura: %d, Items: %d, Linhas: %ld\n", H, N, line_number);
  
  exit(EXIT_SUCCESS);
}
